<?php

class FindRelations
{
    protected $employees =  [
        [
            'id' => 1,
            'skills' => 1,
		    'cost' => 100
        ],
        [
            'id' => 2,
            'skills' => 2,
            'cost' => 400
        ],
        [
            'id' => 3,
            'skills' => 2,
            'cost' => 350
        ],
        [
            'id' => 4,
            'skills' => 3,
            'cost' => 300
        ],
        [
            'id' => 5,
            'skills' => 1,
            'cost' => 200
        ],
        [
            'id' => 6,
            'skills' => 1,
            'cost' => 150
        ],
        [
            'id' => 7,
            'skills' => 3,
            'cost' => 700
        ],
        [
            'id' => 8,
            'skills' => 1,
            'cost' => 80
        ],
        [
            'id' => 9,
            'skills' => 2,
            'cost' => 500
        ],
        [
            'id' => 10,
            'skills' => 1,
            'cost' => 120
        ]
    ];

    protected $tasks = [
        [
            'id' => 1,
            'skills' => 1,
            'estimation' => 2,
        ],
        [
            'id' => 2,
            'skills' => 3,
            'estimation' => 1,
        ],
        [
            'id' => 3,
            'skills' => 3,
            'estimation' => 1,
        ],
        [
            'id' => 4,
            'skills' => 1,
            'estimation' => 1,
        ],
        [
            'id' => 5,
            'skills' => 3,
            'estimation' => 3,
        ],
        [
            'id' => 6,
            'skills' => 2,
            'estimation' => 5,
        ],
        [
            'id' => 7,
            'skills' => 1,
            'estimation' => 3,
        ],
        [
            'id' => 8,
            'skills' => 2,
            'estimation' => 2,
        ],
        [
            'id' => 9,
            'skills' => 2,
            'estimation' => 4,
        ],
        [
            'id' => 10,
            'skills' => 2,
            'estimation' => 2,
        ],
        [
            'id' => 11,
            'skills' => 3,
            'estimation' => 1,
        ],
        [
            'id' => 12,
            'skills' => 1,
            'estimation' => 3,
        ],
        [
            'id' => 13,
            'skills' => 3,
            'estimation' => 5,
        ],
        [
            'id' => 14,
            'skills' => 2,
            'estimation' => 4,
        ],
        [
            'id' => 15,
            'skills' => 3,
            'estimation' => 2,
        ],
        [
            'id' => 16,
            'skills' => 3,
            'estimation' => 1,
        ],
        [
            'id' => 17,
            'skills' => 1,
            'estimation' => 4,
        ],
        [
            'id' => 18,
            'skills' => 2,
            'estimation' => 3,
        ],
        [
            'id' => 19,
            'skills' => 2,
            'estimation' => 6,
        ],
        [
            'id' => 20,
            'skills' => 1,
            'estimation' => 5,
        ],
        [
            'id' => 21,
            'skills' => 3,
            'estimation' => 2,
        ],
        [
            'id' => 22,
            'skills' => 1,
            'estimation' => 3,
        ],
        [
            'id' => 23,
            'skills' => 1,
            'estimation' => 1,
        ],
        [
            'id' => 24,
            'skills' => 2,
            'estimation' => 2,
        ],
        [
            'id' => 25,
            'skills' => 2,
            'estimation' => 3,
        ],
        [
            'id' => 26,
            'skills' => 1,
            'estimation' => 3,
        ],
        [
            'id' => 27,
            'skills' => 1,
            'estimation' => 2,
        ],
        [
            'id' => 28,
            'skills' => 3,
            'estimation' => 2,
        ],
        [
            'id' => 29,
            'skills' => 3,
            'estimation' => 1,
        ],
        [
            'id' => 30,
            'skills' => 3,
            'estimation' => 1,
        ]
    ];

    protected function findEmployee($task)
    {
        $_employees = [];
        foreach ($this->employees as $employee) {
            if ($employee['skills'] == $task['skills']) {
                $_employees[] = $employee;
            }
        }

        if (!empty($_employees)) {
            $minimumCostEmployee = $_employees[0];

            foreach ($_employees as $_employee) {
                if ($minimumCostEmployee['cost'] > $_employee['cost']) {
                    $minimumCostEmployee = $_employee;
                }
            }

            if ($minimumCostEmployee['estimation'] && $minimumCostEmployee['estimation'] > 0) {
                foreach ($_employees as $_employee) {
                    if (!isset($_employee['estimation'])) {
                        $_employee['estimation'] = 0;
                    }
                    if ($minimumCostEmployee['estimation'] > $_employee['estimation']) {
                        $minimumCostEmployee = $_employee;
                    }
                }
            }

            if (!isset($minimumCostEmployee['estimation'])) {
                $minimumCostEmployee['estimation'] = 0;
            }

            return $minimumCostEmployee;
        }

        $_employees = [];
        foreach ($this->employees as $employee) {
            if ($employee['skills'] > $task['skills']) {
                $_employees[] = $employee;
            }
        }

        if (!empty($_employees)) {
            $minimumCostEmployee = $_employees[0];

            foreach ($_employees as $_employee) {
                if ($minimumCostEmployee['cost'] > $_employee['cost']) {
                    $minimumCostEmployee = $_employee;
                }
            }

            if ($minimumCostEmployee['estimation'] && $minimumCostEmployee['estimation'] > 0) {
                foreach ($_employees as $_employee) {
                    if (!isset($_employee['estimation'])) {
                        $_employee['estimation'] = 0;
                    }
                    if ($minimumCostEmployee['estimation'] > $_employee['estimation']) {
                        $minimumCostEmployee = $_employee;
                    }
                }
            }

            if (!isset($minimumCostEmployee['estimation'])) {
                $minimumCostEmployee['estimation'] = 0;
            }

            return $minimumCostEmployee;
        }

        throw new \Exception('Not found');
    }

    protected function setEstimationToEmployee($employee, $task)
    {
        foreach ($this->employees as $key=>$_employee) {
            if ($_employee['id'] == $employee['id']) {
                if (!isset($this->employees[$key]['sum_estimate'])) {
                    $this->employees[$key]['sum_estimate'] = 0;
                }
                $this->employees[$key]['sum_estimate'] += $task['estimation'];
                return $this->employees[$key];
            }
        }
    }

    protected function removeEmployee($minimumCostEmployee)
    {
        foreach ($this->employees as $key=>$employee) {
            if ($minimumCostEmployee['id'] == $employee['id']) {
                unset($this->employees[$key]);
                break;
            }
        }
    }

    public function getRelations()
    {
        $relations = [];
        foreach ($this->tasks as $key=>$task) {
            try {
                $employee = $this->findEmployee($task);
                $_employee = $this->setEstimationToEmployee($employee, $task);
//                $this->removeEmployee($employee);

                $this->tasks[$key]['employee'] = $_employee;
            } catch (Exception $e) {
                $this->tasks[$key]['employee'] = ['error' => 'not found'];
            }
        }

        return $this->tasks;
    }

    public function getEmployees()
    {
        return $this->employees;
    }

    public function getTime()
    {
        $time = 0;
        foreach ($this->employees as $employee) {
            if (isset($employee['sum_estimate'])) {
                $time += $employee['sum_estimate'];
            }
        }

        return $time;
    }

    public function getCost()
    {
        $cost = 0;
        foreach ($this->employees as $employee) {
            if (isset($employee['sum_estimate'])) {
                $cost += ($employee['sum_estimate'] * $employee['cost']);
            }
        }

        return $cost;
    }
}

$obj = new FindRelations;

print_r($obj->getRelations());
//print_r($obj->getEmployees());
echo 'Whole estimate time: ' . $obj->getTime() . "\n";
echo 'Whole estimate cost: ' . $obj->getCost() . "\n";